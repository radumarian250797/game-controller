#ifndef INCFILE4_H_
#define INCFILE4_H_

#include <avr/io.h>
#include "IOlib.h"
GenericIn::GenericIn(unsigned char nr, unsigned char id)
	{
		innr = nr;
		portid = id;
/*
		if (portid == 1)
			DDRA &= ~(1<<innr);
			PORTA |= (1<<innr); 
*/
		if (portid == 2) {
			DDRB &= ~(1<<innr);
			PORTB |= (1<<innr);
		}
		if (portid == 3) {
			DDRC &= ~(1<<innr);
			PORTC |= (1<<innr);
		}
		if (portid == 4) {
			DDRD &= ~(1<<innr);
			PORTD |= (1<<innr);
		}
	}
bool GenericIn::Status()
{
/*
	if (portid = 1)
		if (0 == (PINA & (1<<portnr)))	{
			return true;
		}
		else
		{
			return false;
		}
*/
	if (portid == 2) {
		if ((PINB & (1<<innr)) == 0) {
			return true;
		}
		else {
			return false;
		}
	}
	if (portid == 3) {
		if ((PINC & (1<<innr)) == 0) {
			return true;
		}
		else {
			return false;
		}
	}
	if (portid == 4) {
		if ((PIND & (1<<innr)) == 0) {
			return true;
		}
		else {
			return false;
		}
	}
};

GenericOut::GenericOut(unsigned char nr, unsigned char id)
{
	outnr = nr;
	outid = id;
/*
	if (portid == 1)
		DDRA |= (1<<outnr);
*/
	if (outid == 2) {
		DDRB |= (1<<outnr);
	}
	if (outid == 3) {
		DDRC |= (1<<outnr);
	}
	if (outid == 4) {
		DDRD |= (1<<outnr);
	}
}
void GenericOut::set(bool pwr)
{
/*	
	if (portid == 1) {
		if (pwr == 1) {
			PORTA |= (1<<outnr);
		}
		if (pwr == 0) {
			PORTA &= ~(1<<outnr);
		}
	}	
*/
	if (outid == 2) {
		if (pwr == 1) {
			PORTB |= (1<<outnr);
		}
		if (pwr == 0) {
			PORTB &= ~(1<<outnr);
		}
	}
	if (outid == 3) {
		if (pwr == 1) {
			PORTC |= (1<<outnr);
		}
		if (pwr == 0) {
			PORTC &= ~(1<<outnr);
		}
	}
	if (outid == 4) {
		if (pwr == 1) {
			PORTD |= (1<<outnr);
		}
		if (pwr == 0) {
			PORTD &= ~(1<<outnr);
		}
	}
}

#endif /* INCFILE4_H_ */