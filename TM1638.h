#ifndef INCFILE2_H_
#define INCFILE2_H_


class tm1638
{
	public:
	//! constructor.
	/*! leaves shutdown mode, sets decode mode to code b
		and sets scanmode for 8 digits
	*/
	tm1638();
	//! Method used for shifting single byte (val) to tm1638.
	void shift(uint8_t val);
	
	//! Method used for shifting single byte (val) to tm1638.
	void send(uint8_t digit, uint8_t nr);
	
	//! send single number to specific digit.
	void sendnr(uint8_t adr, uint8_t);
	//! send full number.
	void writenr (long int num);
	
	//! receive 8 bits from dio
	int receivebutt(void);
	
	//! read all buttons
	int readbutts(void);
	
	 //! grab individual button status 1-8
	bool button(uint8_t nr);
	
	private:
	
	// Method the same as "send", but private for internal use.
	void sendcmd(unsigned char adr, unsigned char data);
	
		};

#endif /* INCFILE2_H_ */