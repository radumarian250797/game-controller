
#define F_CPU 8000000
#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/delay.h>
#include "ADCLIB.h" // library for ADC
#include "IOlib.h" // library for IO
#include "TM1638.h" // display library for testing

// initialize analogue controls
ADC_ATMEGA328 LX = ADC_ATMEGA328(3);
ADC_ATMEGA328 LY = ADC_ATMEGA328(2);
ADC_ATMEGA328 RX = ADC_ATMEGA328(5);
ADC_ATMEGA328 RY = ADC_ATMEGA328(4);
// ADC interrupt, triggers when ad conversion is complete
ISR(ADC_vect)
{
	
}


// buttons

GenericIn But1 = GenericIn(0,4);
GenericIn But2 = GenericIn(1,4);
GenericIn But3 = GenericIn(2,4);
GenericIn But4 = GenericIn(3,4);
GenericIn But5 = GenericIn(5,4);
GenericIn But6 = GenericIn(6,4);
GenericIn But7 = GenericIn(7,4);
GenericIn But8 = GenericIn(0,2);
GenericIn AnButL = GenericIn(0,3);
GenericIn AnButR = GenericIn(1,3);


// vibration motor
GenericOut Vib = GenericOut(4,2);

 // display for displaying test output
tm1638 myTM = tm1638();


// floats for analogue sticks
float rightX, rightY, leftX, leftY;


int main(void)
{	Vib.set(false);
	DDRB &= ~(1<<0);
	PORTB |= (1<<0);
	
	RX.calib();
	RY.calib();
	LX.calib();
	LY.calib();
	
	int delayms = 800;
	while (1)
	{
		leftX = LX.adread();
		leftY = LY.adread();
		rightX = RX.adread();
		rightY = RY.adread();

		if (But1.Status()) 
		{
			myTM.writenr(leftX);
			myTM.sendnr(2,1);
			Vib.set(1);
			_delay_ms(100);
			Vib.set(0);
		} 
		if (But2.Status())
		{
			myTM.writenr(leftY);
			myTM.sendnr(2,2);
		}
		if (But3.Status())
		{
			myTM.writenr(rightX);
			myTM.sendnr(2,3);
		}
		if (But4.Status())
		{
			myTM.writenr(rightY);
			myTM.sendnr(2,4);
		}
		if (But5.Status())
		{
			myTM.sendnr(2,5);
		}
		if (But6.Status())
		{
			myTM.sendnr(2,6);
		}
		if (But7.Status())
		{
			myTM.sendnr(2,7);
		}
		if (But8.Status())
		{
			myTM.sendnr(2,8);
		}
		if (AnButR.Status())
		{
			myTM.sendnr(2,9);
		}
		if (AnButL.Status())
		{
			myTM.sendnr(1,1);
			myTM.sendnr(2,0);
		} 
	}
}


