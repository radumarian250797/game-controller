/*
 * TM1638.cpp
 *
 * Created: 30-04-2019 12:47:01
 
 */ 
#define F_CPU 8000000
#ifndef INCFILE3_H_
#define INCFILE3_H_

#include <avr/io.h>
#include "IOlib.h"
#include "TM1638.h"
#include <util/delay.h>

GenericIn  dat = GenericIn(2,2); // prepare pin B2 for read mode.
GenericOut clk = GenericOut(1,2); //clock on pin B1
GenericOut dio = GenericOut(2,2); //set Data out on pin B2
GenericOut stb = GenericOut(3,2); //strobe on pin B3

tm1638::tm1638()
{
		// leave shutdown
		stb.set(0);
		shift(0x44); // set datacmd (4), w. fixed addresses (4)
		stb.set(1);
	
		stb.set(0);
		shift(0x8F); // set displaycmd (8) turn on brightness to max (F)
		stb.set(1);
		
	}
	
void tm1638::shift(uint8_t val)
	{
		
		for (int i = 0; i <= 7; i ++)
		{
			clk.set(0);
				if (((1<<i) & val) > 0 )
					{dio.set(1);}
				else
					{dio.set(0);}
			clk.set(1);
			
		};
	}
	
void tm1638::sendcmd(uint8_t adr, uint8_t data)
	{
		stb.set(0);
		shift(adr);
		shift(data);
		stb.set(1);
		
	}	

void tm1638::send(uint8_t digit, uint8_t nr)
	{
		sendcmd(digit,nr);	
	}

void tm1638::sendnr(uint8_t adr, uint8_t nr)
	{
		int val; // variable to hold hexcode for digit.
		// check which number is to be displayed
		if (nr == 0)
			{ val = 0x3F;}
		if (nr == 1)
			{ val = 0x06;}
		if (nr == 2)
			{ val = 0x5B;}
		if (nr == 3)
			{ val = 0x4F;}
		if (nr == 4)
			{ val = 0x66;}
		if (nr == 5)
			{ val = 0x6D;}
		if (nr == 6)
			{ val = 0x7D;}
		if (nr == 7)
			{ val = 0x07;}
		if (nr == 8)
			{ val = 0x7F;}
		if (nr == 9)
			{ val = 0x6F;}
		stb.set(0);
		shift(0x44); // shift write command
		stb.set(1);
		// check which position. and send digit to position
		if (adr == 1)
			sendcmd(0xC0,val);
		if (adr == 2)
			sendcmd(0xC2,val);
		if (adr == 3)
			sendcmd(0xC4,val);
		if (adr == 4)
			sendcmd(0xC6,val);
		if (adr == 5)
			sendcmd(0xC8,val);
		if (adr == 6)
			sendcmd(0xCA,val);
		if (adr == 7)
			sendcmd(0xCC,val);
		if (adr == 8)
			sendcmd(0xCE,val);
		
	}

void tm1638::writenr(long int num)
{
	for ( uint8_t i = 8; i >= 1; i--)
	{
		int number = num % 10; //mudolo to get lowest digit
		sendnr(i,number); // shift digit to display.
		num = (num / 10); //divide to remove digit that was used.
	};
}

int tm1638::receivebutt(void)
	{
		uint8_t wc = 0; // variable to hold 8 bits
		GenericIn(2,2); // set dio to input
		for (int i = 8, b=1;i; --i, b<<= 1) // i for looping, b for shifting each bit
		{
			clk.set(0); // clock low
			// WAIT for CLK for at least 1usecond
			_delay_us(1);

			if (dat.Status()) //if DIO is high
			{
				wc |= b; // set bit for data to 1
			}
			clk.set(1); // clock high
		}
		
		GenericOut(2,2); // dio as output
		return wc;
		
	}
		
int tm1638::readbutts(void)
	{  
	  long int keys = 0; // variable for output
	  
	  stb.set(1); //strobe high as safety
	  stb.set(0); // strobe low to start sending
	  shift(0x42); // data command, receive buttons
	  
	  for(uint8_t i = 0; i < 4; i++) // for 4 bytes
		{
			keys |= receivebutt() << i; // save recievebutt as individual bytes
		}
		
	  stb.set(1); // strobe high
	  
	  return keys; 
	} 
	
bool tm1638::button(uint8_t nr)
	{
/* if read button value corresponds with the wanted 
button value return true, else return false */
		if (nr == 1)
			{
				if (readbutts() == 1)
					return true;
				else
					return false;
			}
		if (nr == 2)
			{
				if (readbutts() == 2)
					return true;
				else 
					return false;
			}
		if (nr == 3)
			{
				if (readbutts() == 4)
					return true;
				else
					return false;
			}
		if (nr == 4)
			{
				if (readbutts() == 8)
					return true;
				else
					return false;
			}
		if (nr == 5)
			{
				if (readbutts() == 16)
					return true;
				else
					return false;
			}
		if (nr == 6)
			{
				if (readbutts() == 32)
					return true;
				else
					return false;
			}
		if (nr == 7)
			{
				if (readbutts() == 64)
					return true;
				else
					return false;
			}
		if (nr == 8)
			{
				if (readbutts() == 128)
					return true;
				else
					return false;
			}
	}
#endif  /* INCFILE3_H_ */
