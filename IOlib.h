#ifndef INCFILE1_H_
#define INCFILE1_H_

class GenericIn
{
	public:
	GenericIn(unsigned char nr, unsigned char id);
	bool Status();
	private:
	unsigned char innr;
	unsigned char portid;
	};
	
class GenericOut
{
	public:
	GenericOut(unsigned char nr, unsigned char id);
	void set(bool pwr);
	private:
	unsigned char outnr;
	unsigned char outid;
};

#endif /* INCFILE1_H_ */