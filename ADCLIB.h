﻿class ADC_ATMEGA328
{
	public:
	// constructor, Channel sets input pin, level sets function limit
	
	ADC_ATMEGA328(uint8_t channel);
	
	// read value
	uint16_t adread();
	uint16_t lastRead;
	void calib();
	private:
	uint16_t Center;
	uint8_t Channel;
};