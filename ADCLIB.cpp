/*
 * ADC ATMEGA328.cpp
 *
 * Created: 21-05-2019 09:30:10
 * Author : Thomas
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include "ADCLIB.h"



ADC_ATMEGA328::ADC_ATMEGA328(uint8_t channel)
	{
		Center = 500; // set center as 500 untill calibrated
		Channel = channel; // set which channel to read values
		// set Prescaler to 128
		ADCSRA |= (1 << ADPS2) | (1<<ADPS1) | (1<<ADPS0);
		// voltage reference selection. 00 = AREF, with internal Vref turned off.
		ADMUX |= (1 << REFS0); 
		// Enable adc
		ADCSRA |= (1 << ADEN);
		// auto trigger
		ADCSRA |= (0 << ADATE);
		// enable ADC interrupt
		ADCSRA |= (1<<ADIE);
		// select auto trigger mode 000 = freerunning
		ADCSRB = (ADTS2 << 0) | (ADTS1 << 0 ) | (ADTS0 << 0); 
		// enable global interrupts
		sei();
	};

uint16_t ADC_ATMEGA328::adread()
	{
		Channel &= 7;
	 // clear admux bits before OR'ing and then set channel.
		ADMUX = (ADMUX & 0xF0) | (Channel & 0x0F); 
	 // start ADC
		ADCSRA  |= (1<<ADSC);
	 // wait loop while converting
		while(ADCSRA & (1<<ADSC)); 
		lastRead = ADC;
		return (ADC);
	};
	
void ADC_ATMEGA328::calib()
	{
		Center = adread();
		return;
	};





